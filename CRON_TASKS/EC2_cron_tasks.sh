#!/bin/bash
script_path=/home/ec2-user/model
logs_path=/home/ec2-user/model/logs
temp_crontab=/home/ec2-user/model/temp_ec2_crontab

echo '*/1 * * * * cd '$script_path' && python3 model.py >> '$logs_path'/cron_2.txt 2>&1' > $temp_crontab
sudo crontab -u ec2-user $temp_crontab