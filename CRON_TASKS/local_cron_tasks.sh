#!/bin/bash
env=$1
script_path=$2
logs_path=$3
temp_crontab=$4
user=$5

echo "30 01 * * * cd $script_path/AWS && $env batch_HDFS_to_S3.py >> $logs_path/Hadoop/cron_job.txt 2>&1" > $temp_crontab
echo "30 02 * * * cd $script_path/AWS && $env batch_S3_to_Mongo.py >> $logs_path/MongoDB/cron_job.txt 2>&1" >> $temp_crontab
crontab -u $user $temp_crontab