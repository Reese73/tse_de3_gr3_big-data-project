# BLA² Project

**BLA project** aims to classify resume by predicting the most relevant jobs.

Technologies involved : 
- Haddop HDFS
- AWS (EC2 & S3)
- MongoDB

## Our approach : 
1. Download *raw resume data* from HDFS
2. Upload *raw resume data* to AWS S3 bucket
3. Process *raw data* on AWS EC2 instance
4. Upload *processed data* to AWS S3 bucket
5. Download *processed data* to MongoDB
6. Visualize *processed data* with D3.js

# Pre-requisites

* Linux OS
* AWS account
* AWS CLI v2
* Python3
* MongoDB
* Web serveur (Apache2)
* Administrator privileges

# Installation

### 1. Python environement installation
Use requirement.txt to set up your python virtual environement : 
```bash
pip3 install -r requirements.txt
```

### 2. Clone repository
```bash
git clone ssh git@gitlab.com:Reese73/tse_de3_gr3_big-data-project.git
```

### 3. Set-up configuration file
Update configuration file with your workspace preferences.
### Exemple with Haddop HDFS address and path :
```python
[HDFS]
hadoop_cluster_adress = <your_hdfs_cluster_address>:50070
hdfs_path = /user/root/projects/business_data_classification/raw_data/
...
```

### 4. Set-up bash files
Update bash variables with your workspace preferences.

#### 4.1. `instance_config.sh`
parameters to update : 
* `bucket`
```bash
# --- Instance configuration parameters --- #
bucket=<your_bucket_name>
```

#### 4.2. `Launcher.sh`
parameters to update : 
* `env` 
    * *if no virtual environment:* 
        * use python3
    * *if virtual environment is set :*
        1. *In your virtual environement type : `which python3`*
        2. *use the returned path*

* `script_path` : `<your_local_project_path>`

```bash
#!/bin/bash
# --- Launcher parameters --- #
env=<your_local_python3_path>
script_path=<your_local_project_path>
```

### 5. Set-up Hadoop raw_data onn HDFS
Use `<your_local_project_path>/Support_Documents/HELP_Hadoop_commands.md` to manage input raw_data on your hadoop HDFS cluster

# Get started

Start BLA² Launcher :
```bash
cd <your_local_project_path>/
bash Launcher.sh
```
This interface should appear : 
```bash
ADD FINAL INTERFACE
```

## **Select *"option 1"* to lauch project :**
This option should :
* Create AWS S3 bucket
* Upload Model executable on S3 bucket
* Launch AWS EC2 instance
* set cron jobs on EC2 instance
    * Set prediction jobs
* set cron jobs on local Linux machine
    * Set batch from HDFS to S3 bucket
    * Set batch from S3 bucket to MongoDB

## Options

* **Option 2** : should establish an ssh connection with the EC2 instance created
* **Option 3** : allow the user to manually upload a file to the S3 bucket created
* **Option 4** : should delete all file stored in the S3 bucket
* **Option 5** : should start the EC2 instance if stopped
* **Option 6** : should stopped the EC2 instance if started
* **Option 7** : sould shows logs path and last general logs
* **Option 8** : should delete EC2 instance, S3 bucket and cron jobs
* **Option 9** : should exit the BLA² Launcher


# Directory map

### 1. Project directory : `<your_local_project_path>` : 
```
Local project path
|
| _ README.md   : This document
|
| _ Launcher.sh : Application manager - launched by user
|
| _ BLA² - Project Final Review.pdf   : final review document
|
| _ AWS/
|   | _ functions/              : sources functions
|   | _ init_AWS_Cloud.py       : init script - Launched by user with Launcher.sh
|   | _ batch_HDFS_to_S3.py     : batch 1 - launched by local cron job
|   | _ batch_S3_to_mongo.py    : batch 3 - launched by local cron job
|
| _ BlaBlaViz/
|   | _ Client side folder : See associated README.txt
|
| _ config/
|   | _ requirement.txt         : all python modules required to initiate this project
|   | _ project_BLA_config.cfg  : Parameters file (to update)
|   | _ hdfs_add_files.sh       : bash script to add new data on HDFS
|   | _ instance_config.sh      : bash script to be launch at EC2 instance creation (include remote cron job definition)
|
| _ CRON_TASKS/
|   | _ local_cron_tasks.sh : local cron jobs definitions
|   | _ temp_local_crontab  : temporary file used for create cron jobs (do not update)
|
| _ logs/
|   | _ Hadoop/      : cron job logs (batch HDFS -> S3)
|   | _ Launcher/    : errors from Launcher.sh
|   | _ MongoDB/     : cron job logs (batch S3 -> MongoDB)
|   | _ activity.log : general logs
|
| _ ML_model_for_predicting_CVs/
|   | _ model_resources/
|   |
|   | _ Project_codes/
|       | _ jupyter_notebooks/
|       | _ python_scripts/
|
|   | _ Project_datasets/
|
|   | _ Project_outputs/
|       |_ csv_files/
|       |_ json_files/
|       |_ pickled_files/
|           |_ corpus/
|           |_ models/
|           |_ vectorizer/
|       |_ visualizations/
|
| _ Support_Documents/
    | _ HELP_Hadoop_commands.md     : Hadoop usefull commands
```

### 2. S3 Bucket
Directory organisation with actual configuration parameters :
```
S3 bucket name
|
| _ Model/           : Model ressources
| _ toProcess        
|    | _ predict_YEAR-MONTH-DAY.csv : data files to be processed
| _ Processed_data/   :
|    | _ YEAR-MONTH-DAY.csv : files to export in MongDB
| _ Exported_MongoDB/ :
     | _ YEAR-MONTH-DAY.csv : files exported to MongoDB
```

### 3. Local temporary directory
Directory organisation from `<your_local_temp_directory_path>` :
```
Local temporary folder
|
| _ temp/
|   | _ YEAR/
|       | _ MONTH/    
|           | _ DAY/ 
|               | _ all raw data file per date (from HDFS)
| _ toProcess/
|    | _ predict_YEAR-MONTH-DAY.csv : data files to be processed
| _ Processed_data/   :
     | _ YEAR-MONTH-DAY.json : files to export in MongDB (from S3)
```

# Further configuration

### 1. EC2 instance configuration with bash script
If you need to install or modify Linux configuration of the EC2 instance you will need to add bash command to this file : `<your_local_project_path>/config/instance_config.sh`

#### Exemple : 
Add linux package : 
```bash
sudo yum -y install python3
```

Add python3 dependencies : 
```bash
sudo pip3 install boto3
```

Download files from S3 bucket : 
```bash
aws s3 cp s3://S3_object_path> <instance_remote_path>
```

### 3. Cron jobs
if you need to modify local cron jobs you will need to update this file : `<your_local_project_path>/CRON_TASKS/local_cron_tasks.sh`

if you need to modify remote cron jobs you will need to update this file : `<your_local_project_path>/config/instance_config.sh`

```bash
...
# --- EC2 instance cron job creation --- #
echo '30 03 * * * cd '$script_path' && python3 model.py >> '$logs_path'/cron_2.txt 2>&1' > $temp_crontab
```

### Exemples : 
In this exemple `command_1` will be executed by the system every 30 minutes
```bash
*/30 * * * * command_1
```
In this exemple `command_2` will be executed by the system every day at 01:30 AM
```bash
30 01 * * * command_1
```
More informations here : https://crontab.guru/

# *B.L.A.* Developpment teams : 

* **Boksebeld Arthur** *as a front-end data visualsation specialist*

* **Lassiaz Alexandre** *as a AWS cloud specialist*

* **Assontia Loïc** *as a Machine Learning specialist*