import argparse
import os
import pandas as pd
import numpy as np
import re
import string
import gensim
import nltk
import pickle
from nltk.stem import WordNetLemmatizer as wnl
from nltk import word_tokenize
from nltk.corpus import stopwords
from gensim.parsing.preprocessing import STOPWORDS
from numpy import random


def extract_predict_data(df, save_path):

    df_predict = df.groupby("gender").sample(n=50, random_state=1)
    df_predict.drop('categories', axis=1, inplace=True)

    # df_predict.head()

    if not os.path.exists(save_path):
        os.makedirs(os.path.dirname(save_path), exist_ok=True)
    ############## Reindex the Dataframe and Save it HERE ####################
    df_predict.to_csv(save_path, index=True,header=True)
    
    return df_predict


def pre_processing(fp_data, fp_label, fp_categories, fp_save_predict):

    # Lecture des données 
    df_data = pd.read_json(fp_data)
    df_label = pd.read_csv(fp_label)
    df_categories = pd.read_csv(fp_categories)

    # Combinons les 3 dataframes
    df_categories.columns = ['label', 'label_code']
    df_label_cat = df_label.join(df_categories.set_index('label_code')[['label']], on='Category')
    df_join = df_data.join(df_label_cat.set_index('Id')[['label']], on='Id')
    df_join.rename(columns={"label": "categories"}, inplace=True)
    # df_join.head()

    df_predict = extract_predict_data(df_join, fp_save_predict)

    return df_join.drop(df_predict.index)


def clean_text(text):

    #make text lowercase
    text = text.lower()
    #remove html markup (if there is any)
    text = re.sub("((<.*?>))","",text)
    #remove text in square brackets
    text = re.sub('\[.*?\]', '', text)
    #remove punctuation
    text = re.sub('[%s]' % re.escape(string.punctuation), '', text)
    #remove words containing numbers
    text = re.sub('\w*\d\w*', '', text)
    #remove non-ascii and digit
    text = re.sub("(\W|\d+)"," ",text)

    # Create English stop words list
    STOPWORDS = set(stopwords.words('english'))
    # Instance of WordNetLemmatizer class
    lemmatizer = wnl()
    # Tokenize the words of the text
    words_token = word_tokenize(text) 
    # Applying lemmatization on the tokens
    lemmatized = [lemmatizer.lemmatize(word=word, pos='v') for word in words_token]
    # delete stopwords from text
    text = ' '.join(word for word in lemmatized if word not in STOPWORDS) 

    return text

def clean_corpus(df):

    cleaning = lambda x: clean_text(x)

    df['description'] = df['description'].apply(cleaning)

    return df


if __name__ == "__main__":

    
    nltk.download('stopwords')
    nltk.download('punkt')

    
