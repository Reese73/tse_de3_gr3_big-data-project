import pickle5 as pickle
import os
import pandas as pd
import numpy as np
import re
import string
import nltk
import boto3
import s3fs
from datetime import datetime
from nltk.stem import WordNetLemmatizer as wnl
from nltk import word_tokenize
from nltk.corpus import stopwords
from configparser import ConfigParser


def upload_file_to_bucket(bucket_name, file_to_copy, bucket_path, file_name):
    s3_ressource.Object(bucket_name, bucket_path + file_name).upload_file(
    Filename=file_to_copy, ExtraArgs={
        'ACL': 'private',
        'ServerSideEncryption': 'AES256'})
    # log.logger.info("File %s uploaded on Bucket (%s)", file_name, bucket_name)


def isDate(date, delimiter):
    try:
        datetime.strptime(date,"%Y"+delimiter+"%m"+delimiter+"%d")
        return True
    except ValueError:
        return False


def updateConfigFile(section, param, value):
    cfg = ConfigParser()
    cfg.read("./ml_conf.cfg") 
    file = open("./ml_conf.cfg",'w+') 
    cfg.set(section, param, value)
    cfg.write(file)
    file.close()
    # log.logger.debug("Config file modified : %s->%s now is : %s", section, param, value)

def clean_text(text):

    #make text lowercase
    text = text.lower()
    #remove html markup (if there is any)
    text = re.sub("((<.*?>))","",text)
    #remove text in square brackets
    text = re.sub('\[.*?\]', '', text)
    #remove punctuation
    text = re.sub('[%s]' % re.escape(string.punctuation), '', text)
    #remove words containing numbers
    text = re.sub('\w*\d\w*', '', text)
    #remove non-ascii and digit
    text = re.sub("(\W|\d+)"," ",text)

    # Create English stop words list
    STOPWORDS = set(stopwords.words('english'))
    # Instance of WordNetLemmatizer class
    lemmatizer = wnl()
    # Tokenize the words of the text
    words_token = word_tokenize(text) 
    # Applying lemmatization on the tokens
    lemmatized = [lemmatizer.lemmatize(word=word, pos='v') for word in words_token]
    # delete stopwords from text
    text = ' '.join(word for word in lemmatized if word not in STOPWORDS) 

    return text


def clean_corpus(df):

    cleaning = lambda x: clean_text(x)
    df['description'] = df['description'].apply(cleaning)
    return df



if __name__ == "__main__":

    print("Starting prediction")
    
    nltk.download('stopwords')
    nltk.download('punkt')
    
    # Set configuration file
    cfg = ConfigParser() 
    cfg.read("./ml_conf.cfg")
    
    # S3 client creation (objects oreinted)
    s3_ressource = boto3.resource('s3', region_name='us-east-1')
    
    # --- Get configuration Parameters --- #
    # S3 parameters
    last_processed_date = cfg['model']["last_processed_date"]
    bucket_name = cfg['model']["bucket_name"]
    to_processes_folder = cfg['model']["to_processes_folder"]
    processed_data_folder = cfg['model']["processed_data_folder"]
    
    objects = s3_ressource.Bucket(name=bucket_name).objects.filter(Prefix=to_processes_folder)
    for item in objects:
        # Process S3 path name to check date format
        processed_file = item.key
        processed_file_name = item.key.split("/")[1]
        processed_file_date = item.key.split(".")[0][-10:]

        # check if the path is a data folder
        if(isDate(processed_file_date, "-")):
            # log.logger.debug("Data file name (%s) is in a date format", processed_file_date)
            # check if the data folder is already imported
            if datetime.strptime(processed_file_date, "%Y-%m-%d") > datetime.strptime(last_processed_date, "%Y-%m-%d"):
                
                model_filepath = "./selected_model.pkl"
                vect_filepath = "./selected_tfidif_vect.pkl"
                
                predict_filepath = 's3://' + bucket_name + '/' + to_processes_folder + processed_file_name

                # load the model and the vectorizer from disk
                model = pickle.load(open(model_filepath, 'rb'))
                tfidf_vect = pickle.load(open(vect_filepath, 'rb'))

                # Load the predict.csv file from s3
                df_predict = pd.read_csv(predict_filepath)
                df_predict.rename(columns={"Unnamed: 0": "index"}, inplace=True)

                # Clean the predict.csv file
                cleaned = clean_corpus(df_predict)

                description = df_predict.description

                X_test = tfidf_vect.transform(description)

                y_pred = model.predict(X_test)
                
                y_pred = pd.DataFrame(y_pred, columns=['job_predicted'])
                
                output = pd.concat([df_predict, y_pred], axis=1)
                
                fp = './' + processed_file_date + '.csv'
                filename = processed_file_date + '.csv'
                if not os.path.exists(fp):
                    os.makedirs(os.path.dirname(fp), exist_ok=True)
                output.to_csv(fp, index=True,header=True)
                
                upload_file_to_bucket(bucket_name, fp, processed_data_folder, filename)
                
                print("Prediction done!")