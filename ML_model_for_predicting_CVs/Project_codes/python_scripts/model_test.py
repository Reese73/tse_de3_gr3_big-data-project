import pandas as pd
import numpy as np
import os
from numpy import random
import pickle5 as pickle
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.metrics import accuracy_score, confusion_matrix, f1_score
from sklearn.svm import LinearSVC
from sklearn.model_selection import cross_val_score
import seaborn as sns
import matplotlib.pyplot as plt

import re
import string
import gensim

import nltk
from nltk.stem import WordNetLemmatizer as wnl
from nltk import word_tokenize
from nltk.corpus import stopwords
from numpy import random
from gensim.models import Doc2Vec
from sklearn import utils
from gensim.models.doc2vec import TaggedDocument
from tqdm import tqdm

def clean_text(text):

    #make text lowercase
    text = text.lower()
    #remove html markup (if there is any)
    text = re.sub("((<.*?>))","",text)
    #remove text in square brackets
    text = re.sub('\[.*?\]', '', text)
    #remove punctuation
    text = re.sub('[%s]' % re.escape(string.punctuation), '', text)
    #remove words containing numbers
    text = re.sub('\w*\d\w*', '', text)
    #remove non-ascii and digit
    text = re.sub("(\W|\d+)"," ",text)

    # Create English stop words list
    STOPWORDS = set(stopwords.words('english'))
    # Instance of WordNetLemmatizer class
    lemmatizer = wnl()
    # Tokenize the words of the text
    words_token = word_tokenize(text) 
    # Applying lemmatization on the tokens
    lemmatized = [lemmatizer.lemmatize(word=word, pos='v') for word in words_token]
    # delete stopwords from text
    text = ' '.join(word for word in lemmatized if word not in STOPWORDS) 

    return text


def clean_corpus(df):

    cleaning = lambda x: clean_text(x)
    df['description'] = df['description'].apply(cleaning)
    return df

predict_filename = "../../Project_outputs/csv_files/predict.csv"
df_predict = pd.read_csv(predict_filename)
df_predict.head() 

df_predict.rename(columns={"Unnamed: 0": "index"}, inplace=True)
df_predict.head()

a = df_predict.description.apply(lambda x: x.split())
a.head()

a = pd.DataFrame(a, columns=["description"])
a.head()

a = a.description.apply(lambda x: model_dbow.infer_vector(x))
a.head()

type(a)

vectList = []
for vector in a:
    vectList.append(vector)
len(vectList)

vectList[1]

len(vectList[1])

y_predict = logreg.predict(vectList)
y_predict

logreg.predict_proba(vectList)

def df2vectors(df):

    # Clean the predict.csv file
    cleaned = clean_corpus(df)

    description = cleaned.description.apply(lambda x: x.split())

    description = pd.DataFrame(description, columns=["description"])

    description = description.description.apply(lambda x: model_dbow.infer_vector(x))

    vect_list = []
    for vector in description:
        vect_list.append(vector)
    
    return vect_list

cleaned = clean_corpus(df_predict)
cleaned.head()

description = cleaned.description.apply(lambda x: x.split())

description = pd.DataFrame(description, columns=["description"])

description.head()

description = description.description.apply(lambda x: model_dbow.infer_vector(x))

vect_list = []
for vector in description:
    vect_list.append(vector)
print(len(vectList))
print(len(vectList[1]))

y_predict = logreg.predict(vectList)
y_predict


#Read our pre-cleaned data from our first round of cleaning

filename = 'round1_cleaned_corpus_for_models.pkl'
data1 = pickle.load(open(filename, "rb"))
# data = data.transpose()
print("-------")
print(data1.head())
print("-------")

print(" ")

tfidf1 = TfidfVectorizer(sublinear_tf=True, min_df=5, norm='l2')

features1 = tfidf1.fit_transform(data1.description).toarray()
print("-------")
print(features1)
print("-------")

print(" ")

labels1 = data1.categories
print("-------")
print(labels1)
print("-------")

models = [
    RandomForestClassifier(n_estimators=200, max_depth=3, random_state=0),
    LinearSVC(),
    MultinomialNB(),
    LogisticRegression(random_state=0, max_iter=2000),
]
CV = 5
cv_df = pd.DataFrame(index=range(CV * len(models)))
entries = []

for model in models:
    model_name = model.__class__.__name__
    f1_scores = cross_val_score(model, features1, labels1, scoring='f1_macro', cv=CV)
    for fold_idx, f1_score in enumerate(f1_scores):
        entries.append((model_name, fold_idx, f1_score))

cv_df = pd.DataFrame(entries, columns=['model_name', 'fold_idx', 'f1_score'])

print(" ")
print("-------")
print(cv_df.head())
print("-------")

filename = "scores.csv"
if not os.path.exists(filename):
    os.makedirs(os.path.dirname(filename), exist_ok=True)
cv_df.to_csv(filename, index=True,header=True)



sns.boxplot(x='model_name', y='f1_score', data=cv_df)
sns.stripplot(x='model_name', y='f1_score', data=cv_df, 
              size=8, jitter=True, edgecolor="gray", linewidth=2)
plt.savefig('scores.png', dpi=300)   
# plt.show()