# BLA² Project (ML)

## Système de prédiction de métier

* Construction d'un modèle qui assigne la bonne catégorie de métier à un individu sur las base d'un extrait de son CV;
* Comparaison de plusieurs modèles (linear support vector machine, regression logistique, multinomial naive bayes, random forest) pour avoir le meilleur selon deux métriques: le score Macro F1 (f1_score) et le score de disparité démographique moyen  travers les classes (disparate imapct)


### **Les données**:
Nous disposons de 3 jeux de données disponibles dans le dossier [Project_datasets](Project_datasets/). Il s'agit du:
* fichier *data.json* qui contient les extraits de CV et e genre des individus;
* fichier *label.csv* qui contient les labels des métiers associés à chaque individus;
* fichier *categories_string.csv* qui fournit une correspndance entre les labels des métiers et un identifiant (nombre enier).


### **Nettoyage des données**:
Nous avons utilisé deux niveaux de traitement au texte, une en utilisant juste des technqiues basiques (supression des ponctuations, des nombres, ...). Nous avons appelé ce niveau r1 dans nore graphe compilant le résultat des différetns modèles testés. 

Lors du deuxième niveau de traitement (appelé r2) nous supprimons les mots fréquents de al langues anglaise et appliquons aux textes la lemmatisation. Le but est de rentre les extraits de CV le plus neutre afin que lors de la phase d'apprentissage le modèle ne prenne pas compte du enre de l'individu via les textes.

### **Analyse des données**:
<img src="Project_outputs/visualizations/number_of_resumes_by_cataegories.jpg">
Comme le montre le graphe ci-dessus, certains métiers tels que celui de professeur ou encore d'avocat sont fortement surreprésenté comparément aux métiers de rappeur et de dj par exemple.


<img src="Project_outputs/visualizations/Jobs distribution according to the gender.jpg">
Par ailleurs, la figure ci-dessus, nous montre qu'en plus des déséquilibres interclasses, notre jeu de données comportent aussi de déséquilibres intergenre entre classe. Par exemple pour des métiers tels que chirurgien/ne ou encore dentiste, on a beaucoup plus d'hommes que de femmes. Cependant, pour de métiers comme infirmier/ère ou encore de diététicien/ne, nous avons beaucoup plus de femmes que d'hommes. Le seul métier pour lequel le ratio est à peu près d'un est celui de professeur.



### **Construction du modèle**:
Nous avons testé 4 modèles pour ce projet:
* Logistic regression
* Multinomial Naive Bayes
* Linear Support Vector Machine
* Random Forest

Nous les avons évalués en utilisant deux métriques:
* le score Macro F1 (f1_score), pour prendre en compte le déséquilibre inter classe;
*  le score de disparité démographique moyen  travers les classes (disparate impact) pour rendre compte du niveau de "loyauté" de notre selon le genre.

Les résultats ont été compilés dans le graphe ci-dissous:
<img src="Project_outputs/visualizations/recapitulatif.jpg">

Suivant ce modèle nous vons opté pour le modèle comportant la regression logistique et le deuxi!me niveau de traitement de texte. En effet, c'était le seul modèle qui nous peremttait d'avoir un disparate impact le plus proche de 1 possible avec un f1_score acceptable (72%).
