#!/bin/bash
# --- Launcher parameters --- #
env=/opt/anaconda3/envs/tse-grp3-project-bla/bin/python3
script_path=/Users/alexandrelassiaz/Documents/Ressources_TD_TSE/BIG_DATA_PROJECT
user=$(whoami)

# --- Launcher script --- #
logs_path=$script_path/logs
temp_crontab=$script_path/CRON_TASKS/temp_local_crontab
format="+%Y/%m/%d-%H:%M:%S"

log_file=$logs_path/Launcher/$(date +%Y-%m-%d-%H:%M)-Launcher.log
exec 2>> $log_file
echo -e $(date $format) ":: LOG FILE CREATION" >> $log_file

function log() {
    echo -e $(date $format) ":: FAIL :: $1 \nMore in "$log_file
    echo -e $(date $format) ":: FAIL :: $1" >> $log_file
}

while :
do
    clear
    cat<<EOF

------------------------------------------------------------------------------------

    ############################################################################
    ##                          BLA PROJECT                                   ##
    ##               APPLICATION MANAGER : YOUR COCKPIT                       ##
    ############################################################################
    
    Description :
    Please enter an action number

    [1] Initialisation : set-up application components (Admin password is needed)

    [2] Connect to the AWS Linux machine (ssh)

    [3] Upload a new file to AWS S3 Bucket

    [4] Clean AWS S3 Bucket

    [5] Start the AWS Linux machine

    [6] Stop the AWS Linux machine

    [7] See Logs

    [8] Terminate all application components

    [9] Exit

SYSTEM OUT :
EOF
    read -n1 -s
    case "$REPLY" in
    "1")
        echo -e "Processing option 1..."
        if $env $script_path/AWS/init_AWS_Cloud.py; then
            echo -e $(date $format) ":: INFO :: AWS_init_success 1/2"
            if sudo bash $script_path/CRON_TASKS/local_cron_tasks.sh $env $script_path $script_path/logs $temp_crontab $user; then
                echo -e $(date $format) ":: SUCCESS :: AWS_init_success 2/2"
            else
                log init_AWS_fail_LOCAL_CRON
            fi
        else
            log init_AWS_fail_EC2
        fi
    ;;
    "2")
        echo -e "Processing option 2..."
        if $env $script_path/AWS/instances_mgt.py ssh; then
            echo -e $(date $format) ":: SUCCESS :: EC2_ssh_success"
        else
            log EC2_ssh_fail
        fi
    ;;
    "3")  
        echo -e "Processing option 3..."
        file_to_copy=""
        while [[ ! $file_to_copy =~ ^[A-Z:/]. ]]; do
            echo "file to upload : must begin with '/' or 'C:/'"
            read file_to_copy
        done
        file_name=""
        while [[ ! $file_name =~ [a-z-] ]]; do
            echo "file name : must contains only lower letter"
            read file_name
        done
        bucket_path=""
        while [[ ! $bucket_path =~ [a-z] ]]; do
            echo "S3 folder name : must contains only lower letter"
            read bucket_path
        done

        if $env $script_path/AWS/bucket_mgt.py up $file_to_copy $file_name $bucket_path/; then
            echo -e $(date $format) ":: SUCCESS :: S3_upload_success"
        else
            log S3_upload_fail
        fi
    ;;
    "4")
        echo -e "Processing option 4..."
        echo "Delete all files on S3 ?(yes/no)"
        read confirm
        if [ $confirm == "yes" ]; then
            if $env $script_path/AWS/bucket_mgt.py clean; then
                echo -e $(date $format) ":: SUCCESS :: S3_clean_success"
            else
                log S3_clean_fail
            fi
        else
            echo "Action cancelled by user"
        fi
    ;;
    "5")
        echo -e "Processing option 5..."
        if $env $script_path/AWS/instances_mgt.py start; then
            echo -e $(date $format) ":: SUCCESS :: EC2_start_success"
        else
            log EC2_start_fail
        fi
    ;;
    "6")
        echo -e "Processing option 6..."
        if $env $script_path/AWS/instances_mgt.py stop; then
            echo -e $(date $format) ":: SUCCESS :: EC2_stop_success"
        else
            log EC2_stop_fail
        fi
    ;;
    "7")
        echo -e "Processing option 7..."
        echo -e "Launcher Logs are available here   : $logs_path/Launcher"
        echo -e "General Logs are available here    : $script_path/logs\n"
        echo "Last general logs : "
        cat $script_path/logs/activity.log | tail -5
    ;;
    "8")
        echo -e "Processing option 8..."
        if $env $script_path/AWS/bucket_mgt.py clean; then
            if $env $script_path/AWS/bucket_mgt.py delete; then
                echo -e $(date $format) ":: INFO :: AWS_S3_deleted_success 1/3"
            fi
        else
            log AWS_terminate_fail_S3
        fi
        if $env $script_path/AWS/instances_mgt.py terminate; then
            echo -e $(date $format) ":: INFO :: AWS_EC2_terminate_success 2/3"
        else
            log AWS_terminate_fail_EC2
        fi
        echo -e $(date $format) ":: SUCCESS :: AWS_terminate_success 3/3"
        crontab -r              
    ;;
    "9")
        echo -e "Exit..."
        exit                     
    ;;
     * )  
        echo  "Please choose a correct option"
    ;;
    esac
    echo "Press enter key to refresh"
    read
done