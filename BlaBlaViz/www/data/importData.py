import pymongo as pymongo
import pandas as pd


# --- MongoDB functions --- #

## take a file of data, read it to pandas.dataframe, transform it into python dictionnary, and insert it in mongoDB
## @param mongoClient : the client used to query the database
## @param filepath : file to import's path (contains file name)
## @param db : name of database to query
## @param collection  name of collection to query
## @param filetype : type of file to import (only json and csv supported)
def importData(mongoClient, filepath, dbName, collectionName, filetype = 'json'):
    if filetype == 'json':
        data = pd.read_json(filepath)
    elif filetype == 'csv':
        data = pd.read_csv(filepath)
    else:
        raise Exception('Unknown filetype')
    data = data.transpose() 
    data_dict = data.to_dict('records')
    db = mongoClient[dbName]
    col = db[collectionName]
    col.insert_many(data_dict)


# Set MongDB client
client = pymongo.MongoClient("localhost", 27017)

# Import data file from local directory to MongoDB
importData(client, "./data.json", "bigdata_project", "job_prediction", "json")


