//  https://www.positronx.io/how-to-serve-static-files-css-js-images-in-express-js/ => node server setup
const mongoDbConnectionPath = "mongodb://localhost:27017/";
const mongoDbCollectionName = "bigdata_project";
const mongoDbDatabaseName = "job_prediction";

const express = require('express')
const app = express();
const path = require("path");
const mongo = require('mongodb'); 

app.use(express.static(path.join(__dirname, 'www')));
app.use('/www', express.static('www'));

app.get("/getAllJobs", function(req, response) {
    var MongoClient = require('mongodb').MongoClient;
    var url = mongoDbConnectionPath;
    
    MongoClient.connect(url, function(err, db) {
      if (err) throw err;
      var dbo = db.db(mongoDbCollectionName);
      dbo.collection(mongoDbDatabaseName).find().toArray(function(err, result) {
        if (err) throw err;     
        response.json(result);
        db.close();
      });
    }); 
})

var server = app.listen(3000, function() {
    console.log("Running...\n");
});