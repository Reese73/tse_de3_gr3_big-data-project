Ce projet a pour objectif d'interroger une base de données mongoDb hébergeant les résultats de prédictions de CVs par une intelligence artificielle.
Ces données sont utilisées pour construire des visualisations permettant de juger de la pertinence des prédictions de cette IA.

Pour utiliser le projet: 
On suppoe qu'une instance de mongoDb hébergeant une base de données de récpetion des données de prédiction est existante et en marche
- Ouvrir un invité de commande.
- Executer npm install
- Executer node index.js.
- Ouvrir un navigateur
- http://localhost:3000/


Server side :
Serveur nodeJS connecté à mongoDb (chaîne de connexion, collection et base de données interrogée à renseigner des index.js).
www/data/categories_string.csv : métiers appris par l'intelligence artificielle.
www/data/wordcloud/*.csv : fichiers statics utilisés pour construire des nuages de mots pour chaque métier
www/data/importData.py : importe le fichier "data.son" dans mongoDB
www/data/data.json : fichier de données contenant un lot 50 CV traités par le système de recommandation

Client side:
3 visualisations :
- pie chart
- wordcloud
- indicateurs colorés

Le pie chart est construit au chargement de la page :
	- récupère les données (CV avec métier associé et probabilité de chaque métier de correspondre au CV) de mongoDb en Ajax
	- affiche la proportion de chaque métier prédits parmis tous les CVs de la base
	- permet d'avoir une idée de ce qui a été inséré dans la base
	
Le wordcloud permet de constater si le modèle associe les bons mots aux bons métiers :
	- on choisit un métier dans la drop down list
	- récupère les descriptions textuelles nettoyées pour tous les CVs étiquettés avec ce métier
	- construit un nuage de mot avec les descriptions
	- si les mots affichés ne correspondent pas au métier choisi, alors l'IA les prédictions de l'IA pour ce métier sont certainement mauvaises
	Actuellement, les données sources sont stockées en static dans www/data/wordcloud/*.csv.
	Dans le futur, elles seront intégrées au flux de données principals pour être insérées dans la base Mongo.
	
Les indicateurs colorées permettent, via un dégradé du vert au rouge, de constater la confiance qu'à l'IA dans ses prédictions :
	- au chargement de la page les données sont récupérées de mongoDb
	- on regroupe les CVs par métier prédit
	- pour chaque groupe :
		- fait la moyenne des probabilités de chaque CV pour le métier du groupe
		- associe une couleur, du rouge au vert, à cette moyenne : 0.0 = rouge, 1.0 = vert
		- créer un carré de la couleur calculée, avec le métier associé inscrit, ainsi qu'une légende résumant les couleurs
	- cette visualisation permet de voir la pertinence du modèle
	Lorsque le système lit un CV, il donne, pour chaque métier, la probabilité que le CV convienne à ces métiers.
	Le métier prédit pour un CV est le métier qui a la plus grosse probabilité. 
	Cela implique que le modèle peut prédire un métier pour un CV, alors qu'on réalité, plusieurs métiers on de fortes probabilités de convenir.
	Le modèle n'est pas catégorique dans ses prédiction, mais puisque le résultat attendu n'est qu'un seul métier, le modèle est contraint de faire un choix.
	Cette visualisation permet de constater que pour certains métiers, le modèle n'est pas catégorique, n'est pas sûr de lui lorsqu'il prédit ces métiers.
	Si la couleur d'un métier est vert foncé, c'est que le modèle est sûr de lui.
	Si la couleur d'un métier est vert pâle/jaune, ce que le modèle est mitigé : lorsque le modèle prédit ce métier, il hésite probablement avec quelques autres métiers.
	Si la couleur d'un métier est rouge, c'est que se rapproche de l'aléatoire : lorsque le modèle a prédit ce métier, la probabilité était simplement un peu au dessus de la moyenne, mais de peu. 
	Cela signifie qu'on ne peut pas faire confiance au modèle lorsqu'il prédit ce métier pour un CV.
