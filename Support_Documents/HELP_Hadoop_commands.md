# HELP_Hadoop_commands

# Hadoop set-up : 

## HDFS Directory creation
```hdfs dfs -mkdir /user/root/projects```
```hdfs dfs -mkdir /user/root/projects/business_data_classification```
```hdfs dfs -mkdir /user/root/projects/business_data_classification/raw_data```

## HDFS data folder creation
```hdfs dfs -mkdir /user/root/projects/business_data_classification/raw_data/2021/```
```hdfs dfs -mkdir /user/root/projects/business_data_classification/raw_data/2021/02/```
```hdfs dfs -mkdir /user/root/projects/business_data_classification/raw_data/2021/02/12```

## file list
```hdfs dfs -ls /user/root/projects/business_data_classification/raw_data```

# Upload data files from local to HDFS

## 1. Raw data copy from local Linux to HDFS
### Custom bash script to ease copy on HDFS :
```scp -P 2222 ~/Documents/Ressources_TD_TSE/BIG_DATA_PROJECT/config/hdfs_add_files.sh root@sandbox-hdp.hortonworks.com:~/projects/business_data_classification/raw_data```

### raw data files :
```scp -P 2222 ~/Documents/Ressources_TD_TSE/BIG_DATA_PROJECT/SRC/label.csv root@sandbox-hdp.hortonworks.com:~/projects/business_data_classification/raw_data```

```scp -P 2222 ~/Documents/Ressources_TD_TSE/BIG_DATA_PROJECT/SRC/data.json root@sandbox-hdp.hortonworks.com:~/projects/business_data_classification/raw_data```

```scp -P 2222 ~/Documents/Ressources_TD_TSE/BIG_DATA_PROJECT/SRC/categories_string.csv root@sandbox-hdp.hortonworks.com:~/projects/business_data_classification/raw_data```

## file copy within HDFS folders :
```hdfs dfs -cp /user/root/projects/business_data_classification/raw_data/2021/01/04/label.csv /user/root/projects/business_data_classification/raw_data/2021/02/12```

```hdfs dfs -cp /user/root/projects/business_data_classification/raw_data/2021/01/04/data.json /user/root/projects/business_data_classification/raw_data/2021/02/12```

```hdfs dfs -cp /user/root/projects/business_data_classification/raw_data/2021/01/04/categories_string.csv /user/root/projects/business_data_classification/raw_data/2021/02/12```

