import os
import sys
import time
import boto3

import functions.instances_functions as instances_functions
import functions.buckets_functions as buckets_functions
import functions.tools as tools

# Set base directory : project root directory
os.chdir(r"/Users/alexandrelassiaz/Documents/Ressources_TD_TSE/BIG_DATA_PROJECT/")

# Set configuration file
from configparser import ConfigParser
cfg = ConfigParser() 
cfg.read('./config/project_BLA_conf.cfg') 

# --- Set AWS Parameters --- #
# EC2 client creation
ec2_client = boto3.client('ec2')

# --- Get configuration Parameters --- #
# EC2 Parameters
current_instance_ID = cfg['AWS_EC2_INSTANCE']["current_instance_ID"]
current_instance_IP = cfg['AWS_EC2_INSTANCE']["current_instance_IP"]
security_group = cfg['AWS_EC2_INSTANCE']["security_group"]
key = cfg['AWS_S3_BUCKET']["key_path"]

## EC2 instance Management : 
## From this script you can : 
## "start" : start the EC2 instance
## "stop" : stop the EC2 instance
## "terminate" : terminate (permanent delete) the EC2 instance
## "ssh" : establish an ssh connection with the EC2 instance
## REQUIRED : EC2 instance must be created first

action = sys.argv[1]
if (action == "start"):
    instances_functions.start_instance(current_instance_ID)
elif (action == "stop"):
    instances_functions.stop_instance(current_instance_ID)
elif (action == "terminate"):
    instances_functions.terminate_instance(current_instance_ID, security_group)
elif (action == "ssh"):
    instances_functions.ssh_conn_to_instance(key,current_instance_IP)
else:
    print("Please precise your choice : start / stop / terminate / ssh")
    exit()
