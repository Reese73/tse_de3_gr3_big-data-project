import os
import sys
import time
import boto3
import logging

# Set base directory : project root directory
os.chdir(r"/Users/alexandrelassiaz/Documents/Ressources_TD_TSE/BIG_DATA_PROJECT/")

# Logger creation : all level
logger = logging.getLogger('BLA-LOGGER')
logger.setLevel(logging.DEBUG)

# Set logs files and format
fileHandler = logging.FileHandler('logs/activity.log')
fileHandler.setLevel(logging.DEBUG)
log_file_format = logging.Formatter("%(asctime)s — %(levelname)s — %(message)s")
fileHandler.setFormatter(log_file_format)

# initite logger
logger.addHandler(fileHandler)

