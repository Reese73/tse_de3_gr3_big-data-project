import os
import sys
import time
import boto3
from hdfs import InsecureClient
from datetime import datetime
import posixpath as psp

import Loggers as log
import functions.instances_functions as instances_functions
import functions.buckets_functions as buckets_functions
import functions.tools as tools

# Set base directory : project root directory
os.chdir(r"/Users/alexandrelassiaz/Documents/Ressources_TD_TSE/BIG_DATA_PROJECT/")

# Set configuration file
from configparser import ConfigParser
cfg = ConfigParser() 
cfg.read('./config/project_BLA_conf.cfg') 

# --- Set AWS Parameters --- #
# AWS clients creation
ec2_client = boto3.client('ec2')
s3_client = boto3.client('s3')

# AWS client creation (objects oreinted)
ec2_ressources = boto3.resource('ec2', region_name='us-east-1')
s3_ressources = boto3.resource('s3', region_name='us-east-1')

# --- Get configuration Parameters --- #
# HDFS Parameters
hadoop_cluster_adress = cfg['HDFS']["hadoop_cluster_adress"]
hdfs_path = cfg['HDFS']["hdfs_path"]
last_importation_date = cfg['HDFS']["last_importation_date"]
file_1 = cfg['HDFS']["file_1"]
file_2 = cfg['HDFS']["file_2"]
file_3 = cfg['HDFS']["file_3"]

# S3 parameters
bucket_name = cfg["AWS_S3_BUCKET"]["bucket_name"]
local_path = cfg["AWS_S3_BUCKET"]["local_path"]
to_process_S3_folder = cfg['AWS_S3_BUCKET']["to_process_folder"]

# --- batch : HDFS --> S3 --- #

# Set HDFS client
client_hdfs = InsecureClient(hadoop_cluster_adress)

# List of all HDFS directory for a specified path : hdfs_path
hdfs_all_paths = [
  psp.join(path)
  for path, dirs, files in client_hdfs.walk(hdfs_path, depth=4, status=False, ignore_missing=False, allow_dir_changes=False)
]

## Check on each path if it is a data folder or not
## A data path folder must match this format "YYYY/MM/DD"
for i in range(1, len(hdfs_all_paths)):
    hdfs_folder_name = hdfs_all_paths[i][-10:]

    if(tools.isDate(hdfs_folder_name, "/")):
        log.logger.debug("HDFS folder name (%s) is in a date format", hdfs_folder_name)
        if datetime.strptime(hdfs_folder_name, "%Y/%m/%d") > datetime.strptime(last_importation_date, "%Y/%m/%d"):
            log.logger.info("New HDFS data folder found : %s", hdfs_folder_name)

            # Update last importation date into configuration file
            last_importation_date = hdfs_folder_name
            tools.updateConfigFile('HDFS', 'last_importation_date', last_importation_date)

            # Set local directory to temporary store HDFS data
            temp_local_dir = local_path + "temp/"
            if not os.path.exists(temp_local_dir):
                os.makedirs(temp_local_dir)

            # Download data files from HDFS to local directory
            client_hdfs.download(hdfs_path + last_importation_date + "/" + file_1, temp_local_dir , overwrite=True, n_threads=1, temp_dir=None)
            client_hdfs.download(hdfs_path + last_importation_date + "/" + file_2, temp_local_dir , overwrite=True, n_threads=1, temp_dir=None)
            client_hdfs.download(hdfs_path + last_importation_date + "/" + file_3, temp_local_dir , overwrite=True, n_threads=1, temp_dir=None)
            log.logger.info("HDFS data files donwloaded here : %s", temp_local_dir)

            # Create predict.csv file
            file_date = last_importation_date.replace("/", "-")
            perdict_file_name = "predict_" + file_date + ".csv"

            fp_data = temp_local_dir  + file_2
            fp_label = temp_local_dir + file_1
            fp_categories = temp_local_dir + file_3
            fp_save_predict = local_path + to_process_S3_folder + perdict_file_name
            pre_processing(fp_data, fp_label, fp_categories, fp_save_predict)

            # Set S3 path
            bucket_target_path =  to_process_S3_folder

            # Upload predict file from local to S3
            buckets_functions.upload_file_to_bucket(bucket_name, fp_save_predict, bucket_target_path, perdict_file_name)
            log.logger.info("Data files imported in into S3 folder : %s", bucket_target_path)
        else:
            log.logger.debug("HDFS data folder (%s) is already imported", hdfs_folder_name)
    else:
        log.logger.debug("HDFS folder name (%s) is not in a date format", hdfs_folder_name)  

