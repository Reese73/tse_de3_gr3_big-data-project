import os
import sys
import time
import boto3
from hdfs import InsecureClient

import Loggers as log
import functions.instances_functions as instances_functions
import functions.buckets_functions as buckets_functions

import functions.tools as tools

# Set base directory : project root directory
os.chdir(r"/Users/alexandrelassiaz/Documents/Ressources_TD_TSE/BIG_DATA_PROJECT/")

# Set configuration file
from configparser import ConfigParser
cfg = ConfigParser() 
cfg.read('./config/project_BLA_conf.cfg') 

# --- AWS Parameters --- #
# Accès aux services de bas niveaux
ec2_client = boto3.client('ec2')
s3_client = boto3.client('s3')

# Accès au service orienté objet de niveau supérieur
ec2_ressources = boto3.resource('ec2', region_name='us-east-1')
s3_ressources = boto3.resource('s3', region_name='us-east-1')

# --- Configuration Parameters --- #
# EC2 Parameters
old_instance_id = cfg['AWS_EC2_INSTANCE']["current_instance_id"]
key_name = cfg['AWS_EC2_INSTANCE']["key_name"]
ami = cfg['AWS_EC2_INSTANCE']["ami"]
security_group = cfg['AWS_EC2_INSTANCE']["security_group"]
instance_type = cfg['AWS_EC2_INSTANCE']["instance_type"]
instance_config_file = cfg["AWS_EC2_INSTANCE"]["instance_config_file"]

# S3 Parameters
local_path = cfg['AWS_S3_BUCKET']["local_path"]
bucket_name = cfg['AWS_S3_BUCKET']["bucket_name"]
model_S3_folder = cfg['AWS_S3_BUCKET']["model_folder"]


# --- Scripts --- #

# 1. check if the instance is already created and exit if yes
if (old_instance_id != "0"):
    log.logger.warning("Terminate your current instance %s before init anoter one", old_instance_id)
    exit(1)

log.logger.info("AWS CLOUD INITIALISATION START")

# 2. S3 bucket creation
buckets_functions.create_bucket(bucket_name)

# 3. Upload model files
buckets_functions.upload_file_to_bucket(bucket_name, "ML_model_for_predicting_CVs/model_resources/predict.py", model_S3_folder, "predict.py")
buckets_functions.upload_file_to_bucket(bucket_name, "ML_model_for_predicting_CVs/model_resources/ml_conf.cfg", model_S3_folder, "ml_conf.cfg")
buckets_functions.upload_file_to_bucket(bucket_name, "ML_model_for_predicting_CVs/model_resources/selected_modele.plk", model_S3_folder, "selected_modele.plk")
buckets_functions.upload_file_to_bucket(bucket_name, "ML_model_for_predicting_CVs/model_resources/selected_tfidif_vect.plk", model_S3_folder, "selected_tfidif_vect.plk")

# 4. create EC2 instance and upload configuration file
instance_id, public_ip = instances_functions.create_instance(key_name, ami, instance_type, instance_config_file)
tools.updateConfigFile('AWS_EC2_INSTANCE', 'current_instance_ID', instance_id)
tools.updateConfigFile('AWS_EC2_INSTANCE', 'current_instance_IP', public_ip)
