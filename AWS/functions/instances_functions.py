import boto3
import os
from botocore.exceptions import ClientError
import functions.tools as tools
import Loggers as log

# --- Set AWS Parameters --- #
# EC2 client creation
ec2_client = boto3.client('ec2')

# EC2 client creation (objects oreinted) 
ec2_ressources = boto3.resource('ec2', region_name='us-east-1')

# --- EC2 functions --- #

## EC2 instance start
## @param instance_id : id of the instance to start
def start_instance(instance_id):
    if (is_instance_is_in_status(instance_id, "running") == True): 
        log.logger.warning("Current instance %s is already running", instance_id)
        exit(1)
    # Do a dryrun first to verify permissions
    try:
        ec2_client.start_instances(InstanceIds=[instance_id], DryRun=True)
    except ClientError as e:
        if 'DryRunOperation' not in str(e):
            raise

    # Dry run succeeded, run start_instances without dryrun
    try:
        response = ec2_client.start_instances(InstanceIds=[instance_id], DryRun=False)

        # Loop untill EC2 instance state = Running to retrieve its public ip address
        while (is_instance_is_in_status(instance_id, "running") == False): 
            pass
        new_public_ip = get_public_ip(instance_id)
        log.logger.info("Instance %s launched", instance_id)
        tools.updateConfigFile("AWS_EC2_INSTANCE", "current_instance_ip", new_public_ip)
    except ClientError as e:
        log.logger.error("Instance %s : error at start-up", instance_id)
        print(e)

## EC2 instance stop
## @param instance_id : id of the instance to stop
def stop_instance(instance_id):
    if (is_instance_is_in_status(instance_id, "stopped") == True): 
        log.logger.warning("Current instance %s is already stopped", instance_id)
        exit(1)
    # Do a dryrun first to verify permissions
    try:
        ec2_client.stop_instances(InstanceIds=[instance_id], DryRun=True)
    except ClientError as e:
        if 'DryRunOperation' not in str(e):
            raise

    # Dry run succeeded, call stop_instances without dryrun
    try:
        response = ec2_client.stop_instances(InstanceIds=[instance_id], DryRun=False)
        print("EC2 instance stop in progress...")

        # Loop untill EC2 instance state = stopped to update configuration file
        while (is_instance_is_in_status(instance_id, "stopped") == False): 
            pass
        tools.updateConfigFile("AWS_EC2_INSTANCE", "current_instance_ip", "stopped")
        log.logger.info("Instance : %s stopped", instance_id)
    except ClientError as e:
        log.logger.error("Instance %s : error at stop", instance_id)
        print(e)

## EC2 instance terminate or permanent deletion
## @param instance_id : id of the instance to terminate
## @param sec_group : id of the security group associated to the instance to terminate
def terminate_instance(instance_id, sec_grp):
    ec2_client.terminate_instances(InstanceIds=[instance_id])
    print("EC2 instance terminate in progress...")

    # Loop untill EC2 instance state = stopped to update configuration file
    while (is_instance_is_in_status(instance_id, "terminated") == False): 
        pass
    response = ec2_client.delete_security_group(GroupId=sec_grp)
    log.logger.info("Instance : %s terminated", instance_id)
    tools.updateConfigFile('AWS_EC2_INSTANCE', 'current_instance_id', '0')
    tools.updateConfigFile('AWS_EC2_INSTANCE', 'current_instance_ip', '0')
    tools.updateConfigFile('AWS_EC2_INSTANCE', 'security_group', '0')

## Create a AWS Security Group that set in and out rules to allow (SSH / HTTP) access
## Return the security group id
def security_group_create():
    response = ec2_client.describe_vpcs()
    vpc_id = response.get('Vpcs', [{}])[0].get('VpcId', '')
    secGroup = ec2_ressources.create_security_group(GroupName='ec2-sg-bla', Description='EC2 Security Group', VpcId=vpc_id)
    secGroup.authorize_ingress(IpPermissions=[
            {'IpProtocol': 'tcp',
             'FromPort': 80,
             'ToPort': 80,
             'IpRanges': [{'CidrIp': '0.0.0.0/0',}],
             'Ipv6Ranges': [{'CidrIpv6': '::/0',}]},
            {'IpProtocol': 'tcp',
             'FromPort': 22,
             'ToPort': 22,
             'IpRanges': [{'CidrIp': '0.0.0.0/0',}],
             'Ipv6Ranges': [{'CidrIpv6': '::/0',}]}      
        ])
    secGroup.authorize_egress(IpPermissions=[
            {'IpProtocol': 'tcp',
             'FromPort': 80,
             'ToPort': 80,
             'IpRanges': [{'CidrIp': '0.0.0.0/0'}],
             'Ipv6Ranges': [{'CidrIpv6': '::/0',}]},
            {'IpProtocol': 'tcp',
             'FromPort': 22,
             'ToPort': 22,
             'IpRanges': [{'CidrIp': '0.0.0.0/0'}],
             'Ipv6Ranges': [{'CidrIpv6': '::/0',}]}   
        ])
    tools.updateConfigFile("AWS_EC2_INSTANCE", "security_group", secGroup.id)
    return secGroup.id

## Create a EC2 instance and launch it
## Return the instance id and public ip
## @param key_name : name of the AWS key pair associated to the AWS account
## @param ami : AWS AMI code (see AWS EC2)
## @param instance_type : size and type of EC2 instance (see AWS EC2)
## @param security_group : AWS AMI code
def create_instance(key_name, ami,instance_type, instance_config_file):
    security_group_id = security_group_create()

    instance = ec2_ressources.create_instances(
        ImageId=ami,
        MinCount=1,
        MaxCount=1,
        InstanceType=instance_type,
        KeyName=key_name,
        SecurityGroupIds=[security_group_id],
        # Instance_config_file contains a set of bash commands that will be executed at instance creation
        UserData=open(instance_config_file, "r").read(),
        # Attache profile instance : "S3_full_Access" to allow instance to exchange with S3
        IamInstanceProfile={'Arn':'arn:aws:iam::717783363890:instance-profile/S3_full_access'}
    )
    print("EC2 instance creation in progress...")
    instance[0].wait_until_running()
    instance[0].reload()
    instance_id = instance[0].id
    public_ip = instance[0].public_ip_address
    tools.updateConfigFile('AWS_EC2_INSTANCE', 'current_instance_ip', public_ip)
    log.logger.info("Instance : %s created with public IP : %s", instance_id, public_ip)
    return instance_id, public_ip

## Establish a ssh connection to the EC2 instance
## @param key : path to the AWS key pair
## @public_ip : public ip address of the created instance
def ssh_conn_to_instance(key, public_ip):
    os.system('ssh -i %s ec2-user@%s' % (key, public_ip))
    log.logger.info("ssh connection established with %s", public_ip)

## Copy file with scp from local to EC2 instance
## @param key : path to the AWS key pair
## @param fie_to_copy : local path of the file to copy on the instance
## @public_ip : public ip address of the created instance
## @param vm_path : instance target path to copy "file_to_copy"
def ssh_copy_to_instance(key, file_to_copy, public_ip, vm_path):
    #Work only if SSH conection is already established
    os.system('scp -i %s %s ec2-user@%s:%s' % (key, file_to_copy ,public_ip, vm_path))

## Check if EC2 instance state is in a particulary state
## @param instance_id : id of the instance to check its status
## @param status : target status to check
def is_instance_is_in_status(instance_id, status):
    instance = ec2_ressources.Instance(instance_id)
    if instance.state['Name'] == status:
        return True
    else:
        return False

## Get EC2 instance public ip address
## @param instance_id : id of the instance to find public ip address
def get_public_ip(instance_id):
    reservations = ec2_client.describe_instances(InstanceIds=[instance_id]).get("Reservations")
    for reservation in reservations:
        for instance in reservation['Instances']:
            public_ip_adddress = instance.get("PublicIpAddress")
            return public_ip_adddress

