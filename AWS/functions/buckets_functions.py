import os
import boto3
from datetime import datetime
import pymongo

import Loggers as log
import functions.tools as tools
import functions.Mongo_Importation_functions as mongo_functions

# Set base directory : project root directory
os.chdir(r"/Users/alexandrelassiaz/Documents/Ressources_TD_TSE/BIG_DATA_PROJECT/")

# --- Set AWS Parameters --- #
# S3 client creation
s3_client = boto3.client('s3')

# S3 client creation (objects oreinted) 
s3_ressource = boto3.resource('s3', region_name='us-east-1')

# --- Bucket functions --- #

## S3 bucket creation
## @param buccket_name : bucket name must contain only lower characters and "-"
def create_bucket(bucket_name):
    s3_connection=s3_ressource.meta.client
    s3_connection.create_bucket(Bucket=bucket_name)
    log.logger.info("Bucket created, name : %s", bucket_name)
    return bucket_name


## Upload a file to a S3 bucket with encryption
## @param buccket_name : bucket name (must contain only lower characters and "-")
## @param : file_to_copy : path to the file to upload
## @param : bucket_path : path to the S3 bucket (must end with "/")
## @param : file_name : name of the file_to_copy on S3
def upload_file_to_bucket(bucket_name, file_to_copy, bucket_path, file_name):
    s3_ressource.Object(bucket_name, bucket_path + file_name).upload_file(
    Filename=file_to_copy, ExtraArgs={
    'ACL': 'private',
    'ServerSideEncryption': 'AES256'})
    log.logger.info("File %s uploaded on Bucket (%s)", file_name, bucket_name)


## Download a file from a S3 bucket
## @param buccket_name : bucket name must contain only lower characters and "-"
## @param object_name : name of the file to download on S3
## @param : filename : path (including filename) to download
def download_file_from_bucket(bucket_name, object_name, filename):
    s3_client.download_file(bucket_name, object_name, filename)

## Moving objects between two S3 folder
## @param buccket_name : bucket name must contain only lower characters and "-"
## @param old_file_path : object to be moved bucket path 
## @param new_file_path : object target path
def move_files(bucket_name, old_file_path, new_file_path):
    copy_source = {'Bucket': bucket_name, 'Key': old_file_path}

    # step 1 : copy file from old_file_path to new_file_path
    s3_client.copy_object(CopySource = copy_source, Bucket = bucket_name, Key = new_file_path)
    # step 2 : delete file in old_file_path
    s3_client.delete_object(Bucket = bucket_name, Key = old_file_path)

## List all object of a S3 bucket
## @param buccket_name : bucket name must contain only lower characters and "-"
def listBucketFiles(bucket_name):
    for key in s3_client.list_objects(Bucket=bucket_name)['Contents']:
        print(key['Key'])

## Delete all object stored in a S3 bucket
## @param buccket_name : bucket name must contain only lower characters and "-"
def delete_all_objects(bucket_name):
    res = []
    bucket=s3_ressource.Bucket(bucket_name)
    for obj_version in bucket.object_versions.all():
        res.append({'Key': obj_version.object_key,
                    'VersionId': obj_version.id})
    log.logger.debug("Deleted objects : %s", res)
    bucket.delete_objects(Delete={'Objects': res})

## Delete a S3 bucket
## @param buccket_name : bucket name must contain only lower characters and "-"
def delete_bucket(bucket_name):
    s3_client.delete_bucket(Bucket=bucket_name)
    log.logger.info("Bucket %s deleted", bucket_name)

## List all S3 bucket
def display_bucket():
    # Fetch the list of existing buckets
    clientResponse = s3_client.list_buckets()

    # Print the bucket names one by one
    print('Printing bucket names...')
    for bucket in clientResponse['Buckets']:
        print(f'Bucket Name: {bucket["Name"]}')