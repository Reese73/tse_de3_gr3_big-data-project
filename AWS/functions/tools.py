from datetime import datetime
from configparser import ConfigParser
import pandas as pd

import os

import Loggers as log

# Set base directory : project root directory
os.chdir(r"/Users/alexandrelassiaz/Documents/Ressources_TD_TSE/BIG_DATA_PROJECT/")

# --- tools functions --- #

## Update of the configuration file
## @param section : section to update
## @param param : section's parameter to update
## @param value : new value of the section's parameter
def updateConfigFile(section, param, value):
    cfg = ConfigParser()
    cfg.read('./config/project_BLA_conf.cfg') 
    file = open('./config/project_BLA_conf.cfg','w+') 
    cfg.set(section, param, value)
    cfg.write(file)
    file.close()
    log.logger.debug("Config file modified : %s->%s now is : %s", section, param, value)

## Check if a string is in a date format
## @param date : string to check
## @param delimiter : date delimiter between year, month and day
def isDate(date, delimiter):
    try:
        datetime.strptime(date,"%Y"+delimiter+"%m"+delimiter+"%d")
        return True
    except ValueError:
        return False

def extract_predict_data(df, save_path):

    df_predict = df.groupby("gender").sample(n=50, random_state=1)
    df_predict.drop('categories', axis=1, inplace=True)

    # df_predict.head()

    if not os.path.exists(save_path):
        os.makedirs(os.path.dirname(save_path), exist_ok=True)
    ############## Reindex the Dataframe and Save it HERE ####################
    df_predict.to_csv(save_path, index=True,header=True)


def pre_processing(fp_data, fp_label, fp_categories, fp_save_predict):

    # Lecture des données 
    df_data = pd.read_json(fp_data)
    df_label = pd.read_csv(fp_label)
    df_categories = pd.read_csv(fp_categories)

    # Combinons les 3 dataframes
    df_categories.columns = ['label', 'label_code']
    df_label_cat = df_label.join(df_categories.set_index('label_code')[['label']], on='Category')
    df_join = df_data.join(df_label_cat.set_index('Id')[['label']], on='Id')
    df_join.rename(columns={"label": "categories"}, inplace=True)
    # df_join.head()

    extract_predict_data(df_join, fp_save_predict)