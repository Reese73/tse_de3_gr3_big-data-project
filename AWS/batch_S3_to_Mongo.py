import boto3
from datetime import datetime
import os
import pymongo

import Loggers as log
import functions.tools as tools
import functions.Mongo_Importation_functions as mongo_functions
import functions.buckets_functions as buckets_functions

# Set base directory : project root directory
os.chdir(r"/Users/alexandrelassiaz/Documents/Ressources_TD_TSE/BIG_DATA_PROJECT/")

# Set configuration file
from configparser import ConfigParser
cfg = ConfigParser() 
cfg.read('./config/project_BLA_conf.cfg') 

# --- Set AWS Parameters --- #
# S3 clients creation
s3_client = boto3.client('s3')

# S3 client creation (objects oreinted)
s3_ressource = boto3.resource('s3', region_name='us-east-1')

# --- Get configuration Parameters --- #
# S3 parameters
bucket_name = cfg['AWS_S3_BUCKET']["bucket_name"]
Processed_data_S3_folder = cfg['AWS_S3_BUCKET']["Processed_data_folder"]
exported_data_S3_folder = cfg['AWS_S3_BUCKET']["exported_data_folder"]

# MongoDB parameters
local_path = cfg['MongoDB']["local_path"]    
last_exportation_date = cfg['MongoDB']["last_exportation_date"]
port = int(cfg['MongoDB']["db_port"])
host = cfg['MongoDB']["db_host"]
colname = cfg['MongoDB']["db_colname"]
dbname = cfg['MongoDB']["db_name"]
data_format = cfg['MongoDB']["data_format"]

# --- batch : S3 --> MongDB --- #

## List and check if there is a new processed data file in S3
## A processed path folder must match this format "YYYY-MM-DD"
objects = s3_ressource.Bucket(name=bucket_name).objects.filter(Prefix=Processed_data_S3_folder)
for item in objects:
    # Process S3 path name to check date format
    processed_file = item.key
    processed_file_name = item.key.split("/")[1]
    processed_file_date = item.key.split(".")[0][-10:]

    # check if the path is a data folder
    if(tools.isDate(processed_file_date, "-")):
        log.logger.debug("Data file name (%s) is in a date format", processed_file_date)
        # check if the data folder is already imported
        if datetime.strptime(processed_file_date, "%Y-%m-%d") > datetime.strptime(last_exportation_date, "%Y-%m-%d"):
            log.logger.info("New data file found : %s", processed_file_date)
            
            # Download S3 data to temporary directory
            buckets_functions.download_file_from_bucket(bucket_name, processed_file, local_path+processed_file_name)
            log.logger.info("New data file downloaded here : %s", local_path+processed_file_name)
            
            # Update last exportation date into configuration file
            tools.updateConfigFile('MongoDB', 'last_exportation_date', processed_file_date)

            # Move S3 data from "Processed_data" folder to "Exported_MongoDB" folder on S3
            buckets_functions.move_files(bucket_name,processed_file,exported_data_S3_folder+processed_file_name)
            log.logger.debug("Data file moved on S3 : from '%s' to '%s' S3 folder", Processed_data_S3_folder, exported_data_S3_folder)
            
            # Set MongDB client
            client = pymongo.MongoClient(host, port)

            # Import data file from local directory to MongoDB
            mongo_functions.importData(client, local_path+processed_file_name, dbname, colname, data_format)
            log.logger.info("Data file exported into MongoDB")
        else:
            log.logger.debug("New data file is already imported : %s", processed_file_date)
    else:
        log.logger.debug("Data file name (%s) is not in a date format", processed_file_date)