import os
import sys
import time
import boto3

import functions.instances_functions as instances_functions
import functions.buckets_functions as buckets_functions

# Set base directory : project root directory
os.chdir(r"/Users/alexandrelassiaz/Documents/Ressources_TD_TSE/BIG_DATA_PROJECT/")

# Set configuration file
from configparser import ConfigParser
cfg = ConfigParser() 
cfg.read('./config/project_BLA_conf.cfg') 

# --- Set AWS Parameters --- #
# AWS clients creation
ec2_client = boto3.client('ec2')
s3_client = boto3.client('s3')

# AWS client creation (objects oreinted)
s3_ressources = boto3.resource('s3', region_name='us-east-1')

# --- Get configuration Parameters --- #
# S3 Parameters
bucket_name = cfg['AWS_S3_BUCKET']["bucket_name"]

# --- scripts --- #

## S3 Bucket Management : 
## From this script you can : 
## "up" : upload a new file to S3
## "list" : list all objects stored o S3 bucket
## "clean" : delete all objects stored on S3 bucket
## "delete" : delete the S3 bucket (all object must be deleted fisrt)

action = sys.argv[1]

if (action == "up"):
    file_to_copy = sys.argv[2]
    file_name = sys.argv[3]
    bucket_path = sys.argv[4]
    buckets_functions.upload_file_to_bucket(bucket_name, file_to_copy, bucket_path ,file_name) 
elif (action == "list"):
    buckets_functions.listBucketFiles(bucket_name)
elif (action == "clean"):
    buckets_functions.delete_all_objects(bucket_name)
elif (action == "delete"):
    buckets_functions.delete_bucket(bucket_name)
else:
    print("Valide arguments : 'up  <file_path> <bucket_path> <filename>' or 'list' or 'clean' or 'delete'")
    exit()
