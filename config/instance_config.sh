#!/bin/bash
# --- Instance configuration parameters --- #
bucket=tse-de3-project-bla-bucket

# --- EC2 instance start-up script --- #
sudo yum -y upgrade
sudo yum -y install python3
sudo yum -y install gcc
sudo yum -y install python3-devel
sudo pip3 install pandas
sudo pip3 install numpy
sudo pip3 install boto3
sudo pip3 install s3fs
sudo pip3 install seaborn
sudo pip3 install pikle5
sudo pip3 install scikit-learn
sudo pip3 install scipy
sudo pip3 install joblib
sudo pip3 install threadpoolctl
sudo pip3 install nltk
sudo pip3 install gensim
sudo pip3 install ConfigParser
sudo mkdir /home/ec2-user/model
sudo mkdir /home/ec2-user/model/logs
sudo chmod -R 777 /home/ec2-user/model
script_path=/home/ec2-user/model
logs_path=$script_path/logs
temp_crontab=$script_path/temp_ec2_crontab
aws s3 cp s3://$bucket/Model/predict.py $script_path/predict.py
aws s3 cp s3://$bucket/Model/ml_conf.cfg $script_path/ml_conf.cfg
aws s3 cp s3://$bucket/Model/selected_modele.plk $script_path/selected_modele.plk
aws s3 cp s3://$bucket/Model/selected_tfidif_vect.plk $script_path/selected_tfidif_vect.plk

# --- EC2 instance cron job creation --- #
echo '30 3 * * * cd '$script_path' && python3 predict.py >> '$logs_path'/cron_2.txt 2>&1' > $temp_crontab
sudo crontab -u ec2-user $temp_crontab