#!/bin/bash

# Script to run  in HDFS console to simulate new raw data creation : 
# Exemple : 
# $ bash hdfs_add_files.sh
# 
# @param YEAR : year folder name
# @param MONTH : month sub-folder name
# @param DAY : day ub-sub-folder name
# Data are copied from this path : /user/root/projects/business_data_classification/raw_data/2021/01/04/

YEAR=2021
MONTH=03
DAY=21

if [-d /user/root/projects/business_data_classification/raw_data/$YEAR]; then
    if [-d /user/root/projects/business_data_classification/raw_data/$YEAR/$MONTH]; then
        if [-d /user/root/projects/business_data_classification/raw_data/$YEAR/$MONTH/$DAY]; then
            hdfs dfs -cp /user/root/projects/business_data_classification/raw_data/2021/01/04/label.csv /user/root/projects/business_data_classification/raw_data/$YEAR/$MONTH/$DAY
            hdfs dfs -cp /user/root/projects/business_data_classification/raw_data/2021/01/04/data.json /user/root/projects/business_data_classification/raw_data/$YEAR/$MONTH/$DAY
            hdfs dfs -cp /user/root/projects/business_data_classification/raw_data/2021/01/04/categories_string.csv /user/root/projects/business_data_classification/raw_data/$YEAR/$MONTH/$DAY
        else
            hdfs dfs -mkdir /user/root/projects/business_data_classification/raw_data/$YEAR/$MONTH/$DAY
            hdfs dfs -cp /user/root/projects/business_data_classification/raw_data/2021/01/04/label.csv /user/root/projects/business_data_classification/raw_data/$YEAR/$MONTH/$DAY
            hdfs dfs -cp /user/root/projects/business_data_classification/raw_data/2021/01/04/data.json /user/root/projects/business_data_classification/raw_data/$YEAR/$MONTH/$DAY
            hdfs dfs -cp /user/root/projects/business_data_classification/raw_data/2021/01/04/categories_string.csv /user/root/projects/business_data_classification/raw_data/$YEAR/$MONTH/$DAY

        fi
    else 
        hdfs dfs -mkdir /user/root/projects/business_data_classification/raw_data/$YEAR/$MONTH
        hdfs dfs -mkdir /user/root/projects/business_data_classification/raw_data/$YEAR/$MONTH/$DAY
        hdfs dfs -cp /user/root/projects/business_data_classification/raw_data/2021/01/04/label.csv /user/root/projects/business_data_classification/raw_data/$YEAR/$MONTH/$DAY
        hdfs dfs -cp /user/root/projects/business_data_classification/raw_data/2021/01/04/data.json /user/root/projects/business_data_classification/raw_data/$YEAR/$MONTH/$DAY
        hdfs dfs -cp /user/root/projects/business_data_classification/raw_data/2021/01/04/categories_string.csv /user/root/projects/business_data_classification/raw_data/$YEAR/$MONTH/$DAY
    fi
else
    hdfs dfs -mkdir /user/root/projects/business_data_classification/raw_data/$YEAR
    hdfs dfs -mkdir /user/root/projects/business_data_classification/raw_data/$YEAR/$MONTH
    hdfs dfs -mkdir /user/root/projects/business_data_classification/raw_data/$YEAR/$MONTH/$DAY
    hdfs dfs -cp /user/root/projects/business_data_classification/raw_data/2021/01/04/label.csv /user/root/projects/business_data_classification/raw_data/$YEAR/$MONTH/$DAY
    hdfs dfs -cp /user/root/projects/business_data_classification/raw_data/2021/01/04/data.json /user/root/projects/business_data_classification/raw_data/$YEAR/$MONTH/$DAY
    hdfs dfs -cp /user/root/projects/business_data_classification/raw_data/2021/01/04/categories_string.csv /user/root/projects/business_data_classification/raw_data/$YEAR/$MONTH/$DAY
fi